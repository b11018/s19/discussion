// alert("WASSAAAAAP!!!!")

// Conditional Statements

// if statement

let numA = -1

/*
	Syntax:
	if(condition is true){
		statement/code blcok
	};
*/

if(numA < 0){
	console.log('Hi, I am less than 0');
};

console.log(numA < 0);
// result: true

numA = 0

if(numA < 0){
	console.log("Hi I am from numA is 0")
}

console.log(numA < 0)
// result: false

let city = "New York"

if(city === "New York"){
	console.log("Welcome to New York City!");
};

// else if clause

let numH = 1;

if(numA < 0){
	console.log("Hello");

} else if(numH > 0){
	console.log("World");
};

numA = 1

if(numA > 0){
	console.log("Hello");

} else if(numH > 0){
	console.log("World");
};

city = "Tokyo";

if(city === "New York"){
	console.log("Welcome to New York City!");

} else if(city === "Tokyo"){
	console.log("Welcome to Tokyo!")
};

// else statement

if(numA < 0){
	console.log("Hello")

} else if(numH === 0){
	console.log("World");

} else {
	console.log("Again");
};

// else cannot run without if
/*else {
	console.log("Will not run without if");
}; // result: error*/

// else if cannot run without if
/*else if (){
	console.log("Will not run without an if")
} // result: error*/

// conditional statement inside a function

let message = "No message";
console.log(message);

function determinesTyphoonIntensity(windSpeed){

	if(windSpeed < 30){
		return 'Not a typhoon yet'

	} else if(windSpeed <= 61){
		return 'Tropical depression detected'

	} else if(windSpeed >= 62 && windSpeed <= 88){
		return 'Tropical storm detected';

	} else if(windSpeed >= 89 && windSpeed <= 117){
		return  'Severe tropical storm detected';

	} else {
		return 'Typhoon detected'
	};
};

message = determinesTyphoonIntensity(65)
console.log(message);

if(message === "Tropical storm detected"){
	console.warn(message);
};

// Truthy and Falsy

// Truthy Examples
	/*
		1. boolean value of True
		2. 1
	*/

if(true){
	console.log("Boolean true is Truthy");
};

if(1){
	console.log("1 is Truthy");
};

if([]){
	console.log("Blank array is Truthy");
};

let isAdmin = true

if(isAdmin === true){
	console.log("Admin is true");
};

if(isAdmin){
	console.log("Admin is true");
};

// Falsy Examples

if(false){
	console.log("This is Falsy")
};

if(0){
	console.log("0 is Falsy")
};

if(undefined){
	console.log("undefined is Falsy");
};

// Ternary Operators
	/*
		(expression) ? ifTrue : ifFalse;
	*/

// Single statement execution

let ternaryResult = (1 < 18) ? true : false
console.log("Result of Ternary Operator: " + ternaryResult); // result: true

// Multiple statement execution

let name;

function isOfLegalAge(){
	name = "Mark";
	return 'You are of age limit'
};

function isUnderAge(){
	name = "Michelle";
	return 'You are under the age limit'
};

let age = parseInt(prompt('What is your age?'));
console.log(age);

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log('Result of Ternary Operator in functions: ' + legalAge + ', ' + name)

// Switch Statement
	/*
		Syntax:
		switch (expression){
			case value:
				statement;
				break;
			default:
				statement;
				break;
		}
	*/
let day = prompt("What day of the week is it today? ").toLowerCase();
console.log(day);

	switch(day){
		case 'monday' :
		console.log('Color of the day is red');
		break;

		case 'tuesday' :
		console.log('Color of the day is blue');
		break;

		case 'wednesday' :
		console.log('Color of the day is yellow');
		break;

		case 'thursday' :
		console.log('Color of the day is green');
		break;

		case 'friday' :
		console.log('Color of the day is orange');
		break;

		case 'saturday' :
		console.log('Color of the day is pink');
		break;

		case 'sunday' :
		console.log('Color of the day is white');
		break;

		default:
		console.log('Please input a valid day');
		break;
	};

	// Try-Catch-Finally

	function showIntensityAlert(windSpeed){

		try {
			alerat(determinesTyphoonIntensity(windSpeed));

		} catch (error) {
			console.log(typeof error);
			console.warn(error.message);

		} finally {
			alert("Intensity updates will show new alert");
		}
	};

	showIntensityAlert(65);